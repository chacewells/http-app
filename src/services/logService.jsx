import Raven from 'raven-js';

function init() {
  Raven.config("https://0a76d3ad19134ce787ddf9bab6bddddf@sentry.io/1501140",
  {    
    release: '1-0-0',
    environment: 'development-test',
  }).install();
}

function log(error) {
  Raven.captureException(error);
}

export default {
  init,
  log
};

